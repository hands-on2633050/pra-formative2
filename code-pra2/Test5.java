// Formative #5

class Test5 {
public static void main(String[] args)
    {
        int arr[] = { 1, 2, 3 }; // Initialize array

        // final with for-each statement
        for (final int i : arr)
            System.out.print(i + " ");
        }   
}

/*
 * Kesimpulan:
 * Ini adalah foreach untuk array bernaam arr yang menggunakan modifier final
 * artinya dalam loop tidak bisa di re-assign.
 * 
 * Jika mau reassign hilangkan final
 */

