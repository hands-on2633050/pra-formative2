// Formative 4

public class Test2 {
	
    // access modifier tidak ada jadi default (bisa dilihat package)
    int a = 1;
	int b = 2;

	public Test2 func(Test2 obj)
	{
		Test2 obj3 = new Test2();
		obj3 = obj; // obj3 jadi referensi objek yang sama dengan obj (argumen)

        // instance variable obj3 (dan jadinya juga obj) akan di re-assign menjadi obj.a++ + ++obj.b
        // obj.a++ adalah postfix increment jadi obj.a++ return 1 dan variable tersebut beruah menjadi 2
        // ++obj.b adalah prefix increment jadi ++obj.b akan increment variable b dan return (returnnya jadi 3)
        // jadi expresi aritmetik tersebut adalah 2 + 3;
        // obj3.a adalah 5

		obj3.a = obj.a++ + ++obj.b;

        // obj.b diberikan nilainya sendiri, tidak ada yang berubah (3)
		obj.b = obj.b;

        // kesimpulan: return refernsi objek argumen dan increment b 1 dan a menjadi (a) + (b+1)
		return obj3;
	}

	public static void main(String[] args)
	{
		Test2 obj1 = new Test2(); // buat instansi baru Test2

        // func mengambil argumen instansi Test2 dan return refernsinya, jadi obj1 dan obj2 refernsi sama
		Test2 obj2 = obj1.func(obj1); 

        // func dijalankan, jadi:
        // b di increment 1
        // a = 1 + 3 = 4
		System.out.println("obj1.a = " + obj1.a + " obj1.b = " + obj1.b);
		System.out.println("obj2.a = " + obj2.a + " obj1.b = " + obj2.b);

        // kedua object sama karena:
        // metode func menerima argumen obj1 dan mengembalikan variable internal obj3 yang di reassign menjadi referensi argumen
        System.out.println("obj2 = " + obj2);
        System.out.println("obj1 = " + obj1);

	}
}
