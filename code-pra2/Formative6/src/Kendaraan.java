
public class Kendaraan {
    private int roda = 2;
    private String brand;
    private String model;

    Kendaraan(String brand, String model){
        this.setBrand(brand);
        this.setModel(model);
    }

    public String drawBody(){
        return "[____]";
    }

    public String drawWhole(){
        int bodyLength = this.drawBody().length();
        int space =((bodyLength-this.roda) / roda) + 1;

        String bottom = "";
        for(int i = 0; i < roda; i++){
            bottom+= "O"+" ".repeat(space);
        }

        return this.drawBody() + "\n" + bottom;
    }

    public void setBrand(String brand){
        this.brand = brand;
    }

    public void setModel(String model){
        this.model = model;
    }

    public String getBrand(){
        return brand;
    }

    public String getModel(){
        return model;
    }

    @Override
    public String toString() {
        return this.drawWhole();
    }

    public void go(){
        try {
            System.out.println(this + " " + "Merek: " + this.getBrand() + ". Model: " + this.getModel());
            Thread.sleep(2000);
            System.out.println("\n");
            System.out.println("Let's goooo!");
            Thread.sleep(2000);

        } catch (InterruptedException e) {
            System.out.println("Something went wrong...");
        }
    }
}
