

public class SepedaMotor extends Kendaraan implements Bermesin {

    SepedaMotor(String brand, String model){
        super(brand, model);
    }

    @Override
    public void turnOn() {
       try {
            System.out.println("Nyalain mesin dulu bro...");
            Thread.sleep(1000);
       } catch (InterruptedException e) {
            System.out.println("Gak jadi nyala bro...");
       }
    }

    @Override
    public String drawBody() {
        return "/>>>\\/";
    }

    @Override
    public void go() {
        this.turnOn();
        super.go();
        System.out.println("Ngeeeng, ngeeng, ngeeng");
    }
}
