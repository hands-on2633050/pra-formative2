

/*
 * Buat java code yang mencerminkan data object sebuah sepeda & sepeda motor yang masing masing memiliki roda dua. 
 * Hanya saja sepeda motor memiliki mesin pembakaran dan sepeda tidak
*/

public class Formative6 {
    public static void main(String[] args) {
        SepedaMotor sepedaMotor = new SepedaMotor("Yamaha", "Mio");

        Sepeda sepeda = new Sepeda("Super Brand", "Super Model");
        
        System.out.println("\n");     
        sepedaMotor.go();
        System.out.println("\n");     
        sepeda.go();

    }
}
