

public class Sepeda extends Kendaraan {
    Sepeda(String brand, String model){
        super(brand, model);
    }

   @Override
   public String drawBody() {
       return "/-\\|";
   }

   @Override
    public void go() {
        super.go();
        System.out.println("Kring... kring... kring...");
    }
}
