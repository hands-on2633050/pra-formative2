// Formative #1
// Apa hasil dari code berikut jika di jalankan:

public class Test {
    public static void main(String[] args) {
        Test obj = new Test();
        obj.start();

        /*
         * sekuensi penjalanan program:
         * this.start() -> panggil this.method("do") -> this.method("do") print "dogood" (tanpa return line) dan return "do good" ->
         * this.start() melanjutkan alur setelah mendapatkan return value this.method("do") dan print ": do good" 
         */
    }

    public void start() {
        String stra = "do"; // Initialization

        // panggil method jadi "do" ditambah " good" karena method akan secara konstan return " good"
        String strb = method(stra);

        // print string yang diformat sebagaimanarupa
        System.out.print(": "+stra + strb);
    }

    public String method(String stra) {

        // variabel stra menyiman mengubah referensi menjadi -> argumen string ditambah "good"
        stra = stra + "good";

        // print argumen string
        System.out.print(stra);

        // return string biasa
        return " good";
    }
}

